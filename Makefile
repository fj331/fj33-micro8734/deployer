NEXUS_USER="admin"
NEXUS_PASSWORD="admin123"
EXTENSION_DIR="extensions"

build/dist: build/install/plugins
	$(info Remove unnecessary files) 
	rm -rf $(EXTENSION_DIR)

build/install/plugins:
	$(info Installing plugins)
	@ ls -d $(EXTENSION_DIR) | sed -r 's/(\w+)/make -C $(EXTENSION_DIR)\/\1 install/g' | bash 

build/docker:
override MY_IP = "$(shell ip -4 addr show en0 | grep inet | sed -r -e 's/inet\s+(([0-9]{1,3}\.?){4}).*/\1/g' -e 's/\s//g')"
override NEXUS_MIRROR_URL = "http://$(MY_IP):8081/repository/maven-public/"

	$(info $(MY_IP))

	$(info $(NEXUS_MIRROR_URL))

	$(info Building deployer image)
	docker image build --build-arg NEXUS_MIRROR_URL=$(NEXUS_MIRROR_URL) --build-arg NEXUS_USER=$(NEXUS_USER) --build-arg NEXUS_PASSWORD=$(NEXUS_PASSWORD) -t micro8734/deployer .
